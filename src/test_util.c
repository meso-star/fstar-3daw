/* Copyright (C) |Meso|Star> 2015 (contact@meso-star.com)
 *
 * This software is a collection of files whose purpose is to give access
 * to the Star-3DAW library from Fortran programs
 *
 * This software is governed by the CeCILL license under French law and
 * abiding by the rules of distribution of free software. You can use,
 * modify and/or redistribute the software under the terms of the CeCILL
 * license as circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info".
 *
 * As a counterpart to the access to the source code and rights to copy,
 * modify and redistribute granted by the license, users are provided only
 * with a limited warranty and the software's author, the holder of the
 * economic rights, and the successive licensors have only limited
 * liability.
 *
 * In this respect, the user's attention is drawn to the risks associated
 * with loading, using, modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean that it is complicated to manipulate, and that also
 * therefore means that it is reserved for developers and experienced
 * professionals having in-depth computer knowledge. Users are therefore
 * encouraged to load and test the software's suitability as regards their
 * requirements in conditions enabling the security of their systems and/or
 * data to be ensured and, more generally, to use and operate it in the
 * same conditions as regards security.
 *
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms. */

#include "test_util.h"

#include <string.h>

const char* cbox_obj =
    "mtllib cbox.mtl cbox2.mtl\n"
    "v -1.01 0 0.99\n"
    "v 1 0 0.99\n"
    "v 1 0 -1.04\n"
    "v -0.99  0 -1.04\n"
    "g floor\n"
    "usemtl floor\n"
    "f -4 -3 -2 -1\n"

    "v -1.02 1.99 0.99\n"
    "v -1.02 1.99 -1.04\n"
    "v 1 1.99 -1.04\n"
    "v 1 1.99 0.99\n"
    "g ceiling\n"
    "usemtl ceiling\n"
    "f -4 -3 -2 -1\n"

    "v -0.99 0 -1.04\n"
    "v 1 0 -1.04\n"
    "v 1 1.99 -1.04\n"
    "v -1.02 1.99 -1.04\n"
    "g back\n"
    "usemtl back\n"
    "f -4 -3 -2 -1\n"

    "v 1 0 -1.04\n"
    "v 1 0 0.99\n"
    "v 1 1.99 0.99\n"
    "v 1 1.99 -1.04\n"
    "g right\n"
    "usemtl right\n"
    "f -4 -3 -2 -1\n"

    "v -1.01 0 0.99\n"
    "v -0.99 0 -1.04\n"
    "v -1.02 1.99 -1.04\n"
    "v -1.02 1.99 0.99\n"
    "g left\n"
    "usemtl left\n"
    "f -4 -3 -2 -1\n";

const char* cbox_mtl =
    "newmtl left\n"
    "Ns 10\n"
    "Ni 1.5\n"
    "illum 2\n"
    "Ka 0.63 0.065 0.05\n"
    "Kd 0.63 0.065 0.05\n"
    "Ks 0 0 0\n"
    "Ke 0 0 0\n"

    "newmtl right\n"
    "Ns 10\n"
    "Ni 1.5\n"
    "illum 2\n"
    "Ka 0.14 0.45 0.091\n"
    "Kd 0.14 0.45 0.091\n"
    "Ks 0 0 0\n"
    "Ke 0 0 0\n"

    "newmtl floor\n"
    "Ns 10\n"
    "Ni 1\n"
    "illum 2\n"
    "Ka 0.725 0.71 0.68\n"
    "Kd 0.725 0.71 0.68\n"
    "Ks 0 0 0\n"
    "Ke 0 0 0\n"

    "newmtl ceiling\n"
    "Ns 10\n"
    "Ni 1\n"
    "illum 2\n"
    "Ka 0.725 0.71 0.68\n"
    "Kd 0.725 0.71 0.68\n"
    "Ks 0 0 0\n"
    "Ke 0 0 0\n";

const char* cbox2_mtl =
    "newmtl back\n"
    "Ns 10\n"
    "Ni 1\n"
    "illum 2\n"
    "Ka 0.725 0.71 0.68\n"
    "Kd 0.725 0.71 0.68\n"
    "Ks 0 0 0\n"
    "Ke 0 0 0\n";

void create_file(const char *name, const char* data) {
  FILE* file;
  NCHECK(name, NULL);
  NCHECK(data, NULL);
  file = fopen(name, "w");
  NCHECK(file, NULL);
  fwrite(data, sizeof(char), strlen(cbox_obj), file);
  fclose(file);
}
