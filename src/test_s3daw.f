! Copyright (C) |Meso|Star> 2015 (contact@meso-star.com)
!
! This software is a collection of files whose purpose is to give access
! to the Star-3DAW library from Fortran programs
!
! This software is governed by the CeCILL license under French law and
! abiding by the rules of distribution of free software. You can use,
! modify and/or redistribute the software under the terms of the CeCILL
! license as circulated by CEA, CNRS and INRIA at the following URL
! "http://www.cecill.info".
!
! As a counterpart to the access to the source code and rights to copy,
! modify and redistribute granted by the license, users are provided only
! with a limited warranty and the software's author, the holder of the
! economic rights, and the successive licensors have only limited
! liability.
!
! In this respect, the user's attention is drawn to the risks associated
! with loading, using, modifying and/or developing or reproducing the
! software by the user in light of its specific status of free software,
! that may mean that it is complicated to manipulate, and that also
! therefore means that it is reserved for developers and experienced
! professionals having in-depth computer knowledge. Users are therefore
! encouraged to load and test the software's suitability as regards their
! requirements in conditions enabling the security of their systems and/or
! data to be ensured and, more generally, to use and operate it in the
! same conditions as regards security.
!
! The fact that you are presently reading this means that you have had
! knowledge of the CeCILL license and that you accept its terms.

#include <rsys/rsys.inc>

#include <star/s3d.inc>

#include "s3daw.inc"

#define SZT(n) INT(n, C_SIZE_T)

MODULE HELPER

CONTAINS

SUBROUTINE TEST_CBOX(S3DAW)
  USE ISO_C_BINDING, ONLY: C_CHAR, C_INT, C_SIZE_T, C_PTR, C_LOC, C_NULL_PTR, C_ASSOCIATED
  USE RSYS_BINDING
  USE RSYS_STDIO_BINDING
  USE S3DAW_BINDING
  USE TEST_S3DAW_UTIL
  IMPLICIT NONE
  TYPE(C_PTR), INTENT(IN), VALUE :: S3DAW
  TYPE(C_PTR) :: SHAPE
  TYPE(C_PTR) :: FILE
  INTEGER(C_SIZE_T) :: ISHAPE, NSHAPES
  INTEGER(C_INT) :: foo ! to discard unused function returns

  CHECK_TRUE(C_ASSOCIATED(S3DAW))

  CALL CREATE_FILE("cbox.obj", CBOX_OBJ)
  CALL CREATE_FILE("cbox.mtl", CBOX_MTL)

  foo = C_REMOVE("cbox2.mtl")

  CHECK(S3DAW_LOAD(C_NULL_PTR, "cbox.obj"), RES_BAD_ARG)
  CHECK(S3DAW_LOAD(S3DAW, "cbox.obj__"), RES_IO_ERR)
  CHECK(S3DAW_LOAD(S3DAW, "cbox.obj"), RES_OK)

  CALL CREATE_FILE("cbox2.mtl", CBOX2_MTL)

  CHECK(S3DAW_LOAD(S3DAW, "cbox.obj"), RES_OK)

  CHECK(S3DAW_GET_SHAPES_COUNT(C_NULL_PTR, NSHAPES), RES_BAD_ARG)
  CHECK(S3DAW_GET_SHAPES_COUNT(S3DAW, NSHAPES), RES_OK)
  CHECK(NSHAPES, SZT(5))

  CHECK(S3DAW_GET_SHAPE(C_NULL_PTR, SZT(0), SHAPE), RES_BAD_ARG)
  DO ISHAPE = 1, NSHAPES
    CHECK(S3DAW_GET_SHAPE(S3DAW, ISHAPE, SHAPE), RES_OK)
  END DO
  ! Valid range is [1 NSHAPES]
  CHECK(S3DAW_GET_SHAPE(S3DAW, SZT(0), SHAPE), RES_BAD_ARG)
  CHECK(S3DAW_GET_SHAPE(S3DAW, NSHAPES+1, SHAPE), RES_BAD_ARG)

  CHECK(S3DAW_CLEAR(C_NULL_PTR), RES_BAD_ARG)
  CHECK(S3DAW_CLEAR(S3DAW), RES_OK)
  CHECK(S3DAW_GET_SHAPES_COUNT(S3DAW, NSHAPES), RES_OK)
  CHECK(NSHAPES, SZT(0))
  CHECK(S3DAW_GET_SHAPE(S3DAW, SZT(1), SHAPE), RES_BAD_ARG)

  FILE = C_FOPEN("cbox.obj", "r")

  CHECK(S3DAW_LOAD_STREAM(S3DAW, FILE), RES_OK)
  CHECK(S3DAW_GET_SHAPES_COUNT(S3DAW, NSHAPES), RES_OK)
  CHECK(NSHAPES, 5)
  DO ISHAPE = 1, NSHAPES
    CHECK(S3DAW_GET_SHAPE(S3DAW, ISHAPE, SHAPE), RES_OK)
  END DO

  CHECK(C_FCLOSE(FILE), 0)
END SUBROUTINE TEST_CBOX

END MODULE HELPER

PROGRAM TEST
  USE ISO_C_BINDING, ONLY: C_CHAR, C_INT, C_PTR, C_LOC, C_NULL_PTR
  USE RSYS_BINDING
  USE RSYS_MEM_ALLOCATOR_BINDING
  USE S3D_BINDING
  USE S3DAW_BINDING
  USE HELPER
  IMPLICIT NONE
  TYPE(MEM_ALLOCATOR), TARGET :: ALLOCATOR_PROXY
  TYPE(C_PTR) :: AW_MTL
  TYPE(C_PTR) :: AW_OBJ
  TYPE(C_PTR) :: S3DAW
  TYPE(C_PTR), TARGET :: S3D

  CHECK(MEM_INIT_PROXY_ALLOCATOR(ALLOCATOR_PROXY, MEM_DEFAULT_ALLOCATOR), RES_OK)

  CHECK(S3D_DEVICE_CREATE(C_NULL_PTR, C_LOC(ALLOCATOR_PROXY), 0, S3D), RES_OK)

  CHECK(S3DAW_CREATE(C_NULL_PTR, C_NULL_PTR, C_NULL_PTR, C_NULL_PTR, C_NULL_PTR, 1, S3DAW), RES_BAD_ARG)
  CHECK(S3DAW_CREATE(C_NULL_PTR, C_NULL_PTR, C_NULL_PTR, C_NULL_PTR, S3D, 1, S3DAW), RES_OK)

  CHECK(S3DAW_REF_GET(C_NULL_PTR), RES_BAD_ARG)
  CHECK(S3DAW_REF_GET(S3DAW), RES_OK)
  CHECK(S3DAW_REF_PUT(C_NULL_PTR), RES_BAD_ARG)
  CHECK(S3DAW_REF_PUT(S3DAW), RES_OK)
  CHECK(S3DAW_REF_PUT(S3DAW), RES_OK)

  CHECK(S3DAW_CREATE(C_NULL_PTR, C_LOC(ALLOCATOR_PROXY), C_NULL_PTR, C_NULL_PTR, C_NULL_PTR, 1, S3DAW), RES_BAD_ARG)
  CHECK(S3DAW_CREATE(C_NULL_PTR, C_LOC(ALLOCATOR_PROXY), C_NULL_PTR, C_NULL_PTR, S3D, 1, S3DAW), RES_OK)
  CHECK(S3DAW_REF_PUT(S3DAW), RES_OK)

  !CHECK(S3DAW_CREATE(LOGGER_DEFAULT, C_LOC(ALLOCATOR_PROXY), C_NULL_PTR, C_NULL_PTR, S3D, 1, S3DAW), RES_OK)
  CHECK(S3DAW_CREATE(C_NULL_PTR, C_LOC(ALLOCATOR_PROXY), C_NULL_PTR, C_NULL_PTR, S3D, 1, S3DAW), RES_OK)

  CHECK(S3DAW_GET_LOADERS(C_NULL_PTR, C_NULL_PTR, C_NULL_PTR), RES_BAD_ARG)
  CHECK(S3DAW_GET_LOADERS(S3DAW, C_NULL_PTR, C_NULL_PTR), RES_OK) ! Useless
  CHECK(S3DAW_GET_LOADERS(C_NULL_PTR, AW_OBJ, C_NULL_PTR), RES_BAD_ARG)
  CHECK(S3DAW_GET_LOADERS(S3DAW, AW_OBJ, C_NULL_PTR), RES_OK)
  CHECK(S3DAW_GET_LOADERS(C_NULL_PTR, C_NULL_PTR, AW_MTL), RES_BAD_ARG)
  CHECK(S3DAW_GET_LOADERS(S3DAW, C_NULL_PTR, AW_MTL), RES_OK)
  CHECK(S3DAW_GET_LOADERS(C_NULL_PTR, AW_OBJ, AW_MTL), RES_BAD_ARG)
  CHECK(S3DAW_GET_LOADERS(S3DAW, AW_OBJ, AW_MTL), RES_OK)

  CALL TEST_CBOX(S3DAW)

  CHECK(S3DAW_REF_PUT(S3DAW), RES_OK)
  CHECK(S3D_DEVICE_REF_PUT(S3D), RES_OK)

  CALL CHECK_MEMORY_ALLOCATOR(ALLOCATOR_PROXY)
  CALL MEM_SHUTDOWN_PROXY_ALLOCATOR(ALLOCATOR_PROXY)
  CHECK(MEM_ALLOCATED_SIZE(), 0)
END PROGRAM TEST
