! Copyright (C) |Meso|Star> 2015 (contact@meso-star.com)
!
! This software is a collection of files whose purpose is to give access
! to the Star-3DAW library from Fortran programs
!
! This software is governed by the CeCILL license under French law and
! abiding by the rules of distribution of free software. You can use,
! modify and/or redistribute the software under the terms of the CeCILL
! license as circulated by CEA, CNRS and INRIA at the following URL
! "http://www.cecill.info".
!
! As a counterpart to the access to the source code and rights to copy,
! modify and redistribute granted by the license, users are provided only
! with a limited warranty and the software's author, the holder of the
! economic rights, and the successive licensors have only limited
! liability.
!
! In this respect, the user's attention is drawn to the risks associated
! with loading, using, modifying and/or developing or reproducing the
! software by the user in light of its specific status of free software,
! that may mean that it is complicated to manipulate, and that also
! therefore means that it is reserved for developers and experienced
! professionals having in-depth computer knowledge. Users are therefore
! encouraged to load and test the software's suitability as regards their
! requirements in conditions enabling the security of their systems and/or
! data to be ensured and, more generally, to use and operate it in the
! same conditions as regards security.
!
! The fact that you are presently reading this means that you have had
! knowledge of the CeCILL license and that you accept its terms.

#include <rsys/rsys.inc>

MODULE S3DAW_BINDING

! (1): Special warning about some API calls manipulating unsigned C-side
! ---------------------------------------------------------------------
! As unsigned types do not exist Fortran-side, SSP binding makes use of C_INT
! where C_UINT (that doesn't exist) would have been the perfect match
! Doing this garantees the size matches, but exposes to overflows if used
! careless Fortran-side (a 'too big' uint will be treated as negative)
! A workaround is to TRANFER the bytes of these unsigned-stored-in-a-signed
! to int64 (see below)

USE ISO_C_BINDING, ONLY: C_FUNPTR, C_PTR, C_INT, C_FLOAT
IMPLICIT NONE


INTERFACE

!*******************************************************************************
! S3DAW API
! ******************************************************************************
 FUNCTION s3daw_create(logger, allocator, loader_obj, loader_mtl, s3d, verbose, s3daw) BIND(C, NAME='s3daw_create')
  USE ISO_C_BINDING, ONLY: C_INT, C_PTR
  IMPLICIT NONE
  TYPE(C_PTR), INTENT(IN), VALUE :: logger ! May be NULL <=> use default logger
  TYPE(C_PTR), INTENT(IN), VALUE :: allocator ! May be NULL <=> use default allocator
  TYPE(C_PTR), INTENT(IN), VALUE :: loader_obj ! May be NULL <=> Internally create the loader
  TYPE(C_PTR), INTENT(IN), VALUE :: loader_mtl ! May be NULL <=> Internally create the loader
  TYPE(C_PTR), INTENT(IN), VALUE :: s3d
  INTEGER(C_INT), INTENT(IN), VALUE :: verbose ! Define the level of verbosity
  TYPE(C_PTR), INTENT(OUT) :: s3daw
  INTEGER(C_INT) :: s3daw_create
 END FUNCTION s3daw_create

 FUNCTION s3daw_ref_get(s3daw) BIND(C, NAME='s3daw_ref_get')
  USE ISO_C_BINDING, ONLY: C_INT, C_PTR
  IMPLICIT NONE
  TYPE(C_PTR), INTENT(IN), VALUE :: s3daw
  INTEGER(C_INT) :: s3daw_ref_get
 END FUNCTION s3daw_ref_get

 FUNCTION s3daw_ref_put(s3daw) BIND(C, NAME='s3daw_ref_put')
  USE ISO_C_BINDING, ONLY: C_INT, C_PTR
  IMPLICIT NONE
  TYPE(C_PTR), INTENT(IN), VALUE :: s3daw
  INTEGER(C_INT) :: s3daw_ref_put
 END FUNCTION s3daw_ref_put

 FUNCTION s3daw_get_loaders(s3daw, obj, mtl) BIND(C, NAME='s3daw_get_loaders')
  USE ISO_C_BINDING, ONLY: C_INT, C_PTR
  USE RSYS_MEM_ALLOCATOR_BINDING, ONLY: MEM_ALLOCATOR
  IMPLICIT NONE
  TYPE(C_PTR), INTENT(IN), VALUE :: s3daw
  TYPE(C_PTR), INTENT(IN) :: obj ! May be NULL <=> Do not get the obj loader
  TYPE(C_PTR), INTENT(IN) :: mtl ! May be NULL <=> Do not get the obj loader
  INTEGER(C_INT) :: s3daw_get_loaders
 END FUNCTION s3daw_get_loaders

 FUNCTION s3daw_load(s3daw, filename) BIND(C, NAME='s3daw_load')
  USE ISO_C_BINDING, ONLY: C_INT, C_CHAR, C_PTR
  USE RSYS_MEM_ALLOCATOR_BINDING, ONLY: MEM_ALLOCATOR
  IMPLICIT NONE
  TYPE(C_PTR), INTENT(IN), VALUE :: s3daw
  CHARACTER(C_CHAR), INTENT(IN) :: filename(*)
  INTEGER(C_INT) :: s3daw_load
 END FUNCTION s3daw_load

 FUNCTION s3daw_load_stream(s3daw, stream) BIND(C, NAME='s3daw_load_stream')
  USE ISO_C_BINDING, ONLY: C_INT, C_PTR
  USE RSYS_MEM_ALLOCATOR_BINDING, ONLY: MEM_ALLOCATOR
  IMPLICIT NONE
  TYPE(C_PTR), INTENT(IN), VALUE :: s3daw
  TYPE(C_PTR), INTENT(IN), VALUE :: stream
  INTEGER(C_INT) :: s3daw_load_stream
 END FUNCTION s3daw_load_stream

! Remove all loaded shapes
 FUNCTION s3daw_clear(s3daw) BIND(C, NAME='s3daw_clear')
  USE ISO_C_BINDING, ONLY: C_INT, C_CHAR, C_PTR
  USE RSYS_MEM_ALLOCATOR_BINDING, ONLY: MEM_ALLOCATOR
  IMPLICIT NONE
  TYPE(C_PTR), INTENT(IN), VALUE :: s3daw
  INTEGER(C_INT) :: s3daw_clear
 END FUNCTION s3daw_clear

 FUNCTION s3daw_get_shapes_count(s3daw, nshapes) BIND(C, NAME='s3daw_get_shapes_count')
  USE ISO_C_BINDING, ONLY: C_INT, C_SIZE_T, C_PTR
  USE RSYS_MEM_ALLOCATOR_BINDING, ONLY: MEM_ALLOCATOR
  IMPLICIT NONE
  TYPE(C_PTR), INTENT(IN), VALUE :: s3daw
  INTEGER(C_SIZE_T), INTENT(OUT) :: nshapes
  INTEGER(C_INT) :: s3daw_get_shapes_count
 END FUNCTION s3daw_get_shapes_count

 ! WARNING: don't use s3daw_get_shape_0 but s3daw_get_shape
 !
 ! Warning: due to differencies in arrays indexing we introduce s3daw_get_shape
 ! as a true Fortran function using this s3daw_get_shape_0
 ! s3daw_get_shape as the 'indexes start at 1' good property
 FUNCTION s3daw_get_shape_0(s3daw, ishape, shape) BIND(C, NAME='s3daw_get_shape')
  USE ISO_C_BINDING, ONLY: C_INT, C_SIZE_T, C_PTR
  USE RSYS_MEM_ALLOCATOR_BINDING, ONLY: MEM_ALLOCATOR
  IMPLICIT NONE
  TYPE(C_PTR), INTENT(IN), VALUE :: s3daw
  INTEGER(C_SIZE_T), INTENT(IN), VALUE :: ishape ! in [0, s3d_get_shapes_count)
  TYPE(C_PTR), INTENT(OUT) :: shape
  INTEGER(C_INT) :: s3daw_get_shape_0
 END FUNCTION s3daw_get_shape_0

END INTERFACE

CONTAINS

 FUNCTION s3daw_get_shape(s3daw, ishape, shape)
  USE ISO_C_BINDING, ONLY: C_INT, C_SIZE_T, C_PTR
  USE RSYS_MEM_ALLOCATOR_BINDING, ONLY: MEM_ALLOCATOR
  IMPLICIT NONE
  TYPE(C_PTR), INTENT(IN), VALUE :: s3daw
  INTEGER(C_SIZE_T), INTENT(IN), VALUE :: ishape ! in [1, s3d_get_shapes_count]
  TYPE(C_PTR), INTENT(OUT) :: shape
  INTEGER(C_INT) :: s3daw_get_shape
  s3daw_get_shape = s3daw_get_shape_0(s3daw, ishape-1, shape)
 END FUNCTION s3daw_get_shape

END MODULE S3DAW_BINDING
