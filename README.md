# fStar-3DAW

## Overview

fStar-3DAW provides Fortran users with a way to use the Star-3DAW C library.
While most of the types and functions in Star-3DAW are just made accessible through
Fortran's ISO_C_BINDING intrinsic module, part of the Star-3DAW features are provided
as C-preprocessor macros and others are implemented as Fortran procedures.

## Install

### Pre-requisites

fStar-3DAW is compatible GNU/Linux as well as Microsoft Windows 7 and later, both
in 64-bits. It was successfully built with the [GNU Compiler 
Collection](https://gcc.gnu.org) (versions 4.7 and later), with the flags:
[-std=f2008ts -ffree-form 
-ffree-line-length-none](https://gcc.gnu.org/onlinedocs/gfortran/Fortran-Dialect-Options.html)
(Fortran 2008 standard including the additions of TS 29113 and TS 18508, free form layout, 
and no limit on source file's line length),
[-cpp](https://gcc.gnu.org/onlinedocs/gfortran/Preprocessing-Options.html)
(C-preprocessing enabled on source files).
No compiler was tested on Windows; it is assumed that Intel Fortran could be 
successfully used.
It relies on [CMake](http://www.cmake.org) and the 
[RCMake](https://gitlab.com/vaplv/rcmake/) package to build. 
It also depends on the [RSys](https://gitlab.com/vaplv/rsys/) and 
[Star-3DAW](https://gitlab.com/meso-star/star-3daw/) libraries.

### How to build

First ensure that CMake and a C and a Fortran compiler with Fortran 2008 + 
TS 29113 support (i.e. gfortran 4.7 or later) are installed on your system. 
Then install the [RCMake](https://gitlab.com/vaplv/rcmake.git) package as 
well as the [RSys](https://gitlab.com/vaplv/rsys.git) and the
[Star-3DAW](https://gitlab.com/meso-star/star-3daw.git) libraries.
Finally, generate the project from the `cmake/CMakeLists.txt` file by 
appending to the `CMAKE_PREFIX_PATH` variable the `<RCMAKE_DIR>/lib/cmake`, 
`<STAR3DAW_DIR>` and `<RSYS_DIR>` directories, where `<RCMAKE_DIR>`, `<STAR3DAW_DIR>` 
and `<RSYS_DIR>` are the install directories of the RCMake package, the Star-3DAW 
and the RSys libraries, respectively.
The resulting project can be edited, built, tested and installed as any CMake 
project (Refer to the [CMake documentation](https://cmake.org/documentation) 
for further informations on CMake).

Example on a GNU/Linux system:

    ~ $ git clone https://gitlab.com/meso-star/fstar-3daw.git
    ~ $ mkdir fstar-3daw/build && cd fstar-3daw/build
    ~/fstar-3daw/build $ cmake -G "Unix Makefiles" \
    > -DCMAKE_PREFIX_PATH="<RCMAKE_DIR>/lib/cmake;<RSYS_DIR>;<STAR3DAW_DIR>" \
    > -DCMAKE_INSTALL_PREFIX=<FSTAR3DAW_INSTALL_DIR> \
    > ../cmake
    ~/fstar-3daw/build $ make && make test
    ~/fstar-3daw/build $ make install

with `<FSTAR3DAW_INSTALL_DIR>` the directory in which fstar-3daw is going to be
installed.

### How to use

Using fstar-3daw requires a fortran compiler supporting the Fortran 2008 standard
and TS 29113 addition, free form layout, unlimited line length for source 
code, and C-preprocessing of Fortran files.

When using gfortran, the adequate options are -std=f2008ts -ffree-form 
-ffree-line-length-none -cpp.

Provided include files (*.inc) must be included using #include, as the Fortran 
INCLUDE statement doesn't allow C-like preprocessor directives in included 
files.

For an example of use, please look at the provided test programs.

## License

fStar-3D is Copyright (C) |Meso|Star> 2015-2016 (<contact@meso-star.com>). It
is a free software released under the [OSI](http://opensource.org)-approved
CeCILL license. You are welcome to redistribute it under certain conditions;
refer to the COPYING files for details.
